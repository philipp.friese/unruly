# Unruly

## What is Unruly

Unruly is a game developed by Adolfo Zanellati [1], where a given $`n \times m`$ matrix has to be colored in black and white, while satisfying the following two conditions:

1. No row or column may contain three consecutive squares of the same color. 
2. Each row and column must contain the same number of black and white fields

## What is in this Repo

This repository contains a Swift implementation of Unruly, including a graphical interface, import of pre-defined fields, export of created playing fields as well as an automated solving mechanism.

This solver works by encoding the field and rules as a SAT-problem and solves this formula using a SAT Solver, namely picosat [2].

## How to play it

After opening the application, you will see an empty grey field. 

To properly play a game, use the menu File -> Open to load one of the provided example files (stored in `example_fields`).

Colored squares does not satisfying the above conditions are marked with a red background.

To solve the game, use Game -> Solve. This will invoke picosat to solve the field. 

Beware: Solving large fields may result in degraded performance based on your hardware.

## Why the hell

This was one part of a University lecture I took at Ludig-Maximilians-Universität (LMU) München, namely SAT Solving by Dr. Jan Johannsen [3].

## Structure

The project is structured as follows:

- **Unruly.xcodeproj** is the Xcode-Project file which is only useful for opening in Xcode. This file can be ignored
- **Unruly**: Main Directory
  - **AppDelegate.swift**: Main hook file for the application
  - **NewGameViewController.swift**: ViewController file used for controlling the "New Game" popup; mainly UI
  - **ViewController.swift**: ViewController file for the main view. This file provides all necessary hooks for reacting to menu-button presses such as "open file" or "solve"; mainly UI
  - **UnrulyRenderer.swift**: Custom ViewController file providing a means for rendering the Unruly Field;  mainly UI
  - **Unruly.swift**: Most important file for the project.
    - **Field** struct: convenience wrapper for containing both data and functions such as:
      - *toCNF() -> [[Int32]]*, returns the DIMACS/ PicoSAT compatible CNF representation of the current Unruly-Field-Problem. This function does the heavy lifting concerning conversion to cnf
      - *validate()*, only validates the Unruly-Field by using higher language features (such as count(*white* and *black* cells)). This function is only used for graphically showing an invalid Unruly-Field 
      - < multiple convenience conversion and load/save functions >
    - **Cell** struct: Each cell is associated with multiple attributes, such as *type*, *locked* (indicating a file-loaded cell) and *valid* (see validate())
    - **CellType** enum: convenience wrapper for cell types
    - **Unruly** Class: Main Unruly-Object class, wraps both the current *field* and *originalField*, the former representing the currently displayed mutable field, the latter representing e.g. the file-loaded field (used among others for the reset feature)
      - *solve(...) -> (Field?, Int)*: tries to solve the current *field* by converting to CNF and running PicoSAT
        - *deref(...) -> [Int]* dereferences literals from the current picosat instance
        - *nextSolution(...) -> [Int]?* iterates through all solutions by blocking (i.e. adding) the previous satisfying assignment
        - PicoSAT is included in this project (as swift can bridge-import cpp and c files) and can thereby be called directly
    - **Unruly-Bridging-Header.h** bridges the picosat-files into the swift project
    - **Assets.xcassets**: App Icon container
    - **Base.lproj**: Language/ Translation container
    - **Info.plist**: "Settings/ INI" file
    - **picosat** Directory: contains picosat
    - **Unruly.entitlements**: Entitlements/ security settings config file



## Benchmark

The application performs reasonably quick, the following measurements have been made:

| Size   | Time       |
| ------ | ---------- |
| Small  | \~ $`3ms`$   |
| Medium | \~ $`20ms`$  |
| Big    | \~ $`60ms`$  |
| Huge   | \~ $`350ms`$ |

All measurements have been taken *after* all literals have been loaded into picosat; The timer has been started right before *picosat_sat(...)* and just after the solution has been dereferenced.


## Links

[1] https://www.chiark.greenend.org.uk/~sgtatham/puzzles/doc/unruly.html

[2] http://fmv.jku.at/picosat/

[3] https://www.tcs.ifi.lmu.de/mitarbeiter/jan-johannsen

//
//  Unruly.swift
//  Unruly
//
//  Created by Philipp Friese on 27.10.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

import Foundation

struct Field {
    var dimension: [Int]
    var fieldData: [[Cell]]
    
    init(dimension: [Int], fieldData: [[Cell]]?) {
        self.fieldData = []
        self.dimension = dimension
        if dimension != [] && (fieldData == nil || fieldData == []) {
            for _ in 0..<dimension[0] {
                let column = Array<Cell>(repeating: Cell(type: .none, locked: false, valid: true, pressed: false), count: dimension[1])
                self.fieldData.append(column)
            }
        } else {
            self.fieldData = fieldData!
        }
    }
    
    func toCNF() -> [[Int32]] {
        var nV: Int32 = 0
        
        func convert(row: Int, column: Int, negated: Bool = false) -> Int32 {
            return (1 + Int32(column) + Int32(dimension[0]) * Int32(row)) * (negated ? -1 : 1)
        }
        
        func getVariable(negated: Bool = false) -> Int32 {
            let v = (1 + Int32(dimension[0] * dimension[1]) + nV) * (negated ? -1 : 1)
            nV += 1
            return v
        }
        
        var clauses: [[Int32]] = []
        
        /** Add "Cell Exists" Rule **/
        for row in 0..<dimension[0] {
            for column in 0..<dimension[1] {
                clauses.append([convert(row: row, column: column, negated: false),
                                convert(row: row, column: column, negated: true)])
            }
        }
        
        /** Add Unit Clauses for existing cells **/
        for row in 0..<dimension[0] {
            for column in 0..<dimension[1] {
                if      fieldData[row][column].type == .black { clauses.append([convert(row: row, column: column, negated: true)])}
                else if fieldData[row][column].type == .white { clauses.append([convert(row: row, column: column, negated: false)]) }
            }
        }
        
        /** Add "No Three Adjacent Equal Cells" Rule Row-Wise **/
        for row in 0..<dimension[0] {
            for column in 0..<(dimension[1] - 2) {
                clauses.append([
                    convert(row: row, column: column, negated: true),
                    convert(row: row, column: column+1, negated: true),
                    convert(row: row, column: column+2, negated: true)
                    ])
                clauses.append([
                    convert(row: row, column: column, negated: false),
                    convert(row: row, column: column+1, negated: false),
                    convert(row: row, column: column+2, negated: false)
                    ])
            }
        }
        
        /** Add "No Three Adjacent Equal Cells" Rule Column-Wise **/
        for column in 0..<dimension[1] {
            for row in 0..<(dimension[0] - 2) {
                clauses.append([
                    convert(row: row, column: column, negated: true),
                    convert(row: row+1, column: column, negated: true),
                    convert(row: row+2, column: column, negated: true)
                    ])
                clauses.append([
                    convert(row: row, column: column, negated: false),
                    convert(row: row+1, column: column, negated: false),
                    convert(row: row+2, column: column, negated: false)
                    ])
            }
        }
        
        /** Add "Equal Number of Black and White Cells" rule
         This uses the 'At-Most-k' constraint for each row and each column with k being set to width/2 and height/2 respectively
         
         This implementation is based on C. Sinz's Sequential Counter,
         shown in "Towards an Optimal CNF Encoding of Boolean Cardinality Constraints" (https://doi.org/10.1007/11564751_73)
         **/
        func generateRegisterCountingRow(inverted: Bool = false) {
            let atMost = Int(floor(Double(dimension[0]/2)))
            for row in 0..<dimension[0] {
                var registers: [[Int32]] = []
                for _ in 0..<dimension[1] {
                    var register: [Int32] = []
                    for _ in 1...atMost { register.append(getVariable()) }
                    registers.append(register)
                }
                
                clauses.append([convert(row: row, column: 0, negated: inverted), registers[0][0]])
                
                for j in 1..<atMost {
                    clauses.append([-1*registers[0][j]])
                }
                for i in 1..<dimension[1]-1 {
                    clauses.append([convert(row: row, column: i, negated: inverted), registers[i][0]])
                    clauses.append([-1*registers[i-1][0], registers[i][0]])
                    for j in 1..<atMost {
                        clauses.append([convert(row: row, column: i, negated: inverted), -1*registers[i-1][j-1], registers[i][j]])
                        clauses.append([-1*registers[i-1][j], registers[i][j]])
                    }
                    clauses.append([convert(row: row, column: i, negated: inverted), -1*registers[i-1][atMost-1]])
                }
                clauses.append([convert(row: row, column: dimension[1]-1, negated: inverted), -1*registers[dimension[1]-2][atMost-1]])
            }
        }
        func generateRegisterCountingColumn(inverted: Bool = false) {
            let atMost = Int(floor(Double(dimension[1]/2)))
            for column in 0..<dimension[1] {
                var registers: [[Int32]] = []
                for _ in 0..<dimension[0] {
                    var register: [Int32] = []
                    for _ in 1...atMost {
                        register.append(getVariable())
                    }
                    registers.append(register)
                }
                
                clauses.append([convert(row: 0, column: column, negated: inverted), registers[0][0]])
                
                for j in 1..<atMost {
                    clauses.append([-1*registers[0][j]])
                }
                for i in 1..<dimension[0]-1 {
                    clauses.append([convert(row: i, column: column, negated: inverted), registers[i][0]])
                    clauses.append([-1*registers[i-1][0], registers[i][0]])
                    
                    for j in 1..<atMost {
                        clauses.append([convert(row: i, column: column, negated: inverted), -1*registers[i-1][j-1], registers[i][j]])
                        clauses.append([-1*registers[i-1][j], registers[i][j]])
                    }
                    
                    clauses.append([convert(row: i, column: column, negated: inverted), -1*registers[i-1][atMost-1]])
                }
                clauses.append([convert(row: dimension[0]-1, column: column, negated: inverted), -1*registers[dimension[0]-2][atMost-1]])
            }
        }
        
        generateRegisterCountingRow(inverted: false)
        generateRegisterCountingRow(inverted: true)
        
        generateRegisterCountingColumn(inverted: false)
        generateRegisterCountingColumn(inverted: true)
        
        return clauses
    }
    mutating func fromCNF(solution: [Int]) {
        for i in 0..<solution.count {
            let pos = decode(location: abs(solution[i])-1)
            
            set(at: pos.0, column: pos.1, cell: Cell(type: solution[i] > 0 ? .white : .black, locked: true, valid: false, pressed: false))
        }
        validate()
    }
    
    mutating func validate(){
        /** Reset every cell **/
        for column in 0..<dimension[0] {
            for row in 0..<dimension[1] {
            fieldData[row][column].valid = true
            }
        }
        
        /** Row based cardinality check **/
        for row in 0..<dimension[0] {
            var countBlack = 0, countWhite = 0
            for column in 0..<dimension[1] {
                if      fieldData[row][column].type == CellType.white { countWhite += 1 }
                else if fieldData[row][column].type == CellType.black { countBlack += 1 }
            }
            
            if countBlack > dimension[0]/2 || countWhite > dimension[0]/2 {
                for column in 0..<dimension[1] {
                    fieldData[row][column].valid = false
                }
            }
        }
        
        /** Column based cardinality check **/
        for column in 0..<dimension[1] {
            var countBlack = 0, countWhite = 0
            for row in 0..<dimension[0] {
                if       fieldData[row][column].type == CellType.white { countWhite += 1 }
                else if  fieldData[row][column].type == CellType.black { countBlack += 1 }
            }
            
            if countBlack > dimension[1]/2 || countWhite > dimension[1]/2 {
                for row in 0..<dimension[0] {
                    fieldData[row][column].valid = false
                }
            }
        }
        
        /** Row based consecutive check **/
        for row in 0..<(dimension[0] - 2) {
            for column in 0..<(dimension[1]) {
                if  fieldData[row][column] ==  fieldData[row + 1][column] &&  fieldData[row + 1][column] ==  fieldData[row + 2][column] {
                    fieldData[row  ][column].valid = false
                    fieldData[row+1][column].valid = false
                    fieldData[row+2][column].valid = false
                }
            }
        }
        
        /** Column based consecutive check **/
        for column in 0..<(dimension[1] - 2) {
            for row in 0..<(dimension[0]) {
                if  fieldData[row][column] ==  fieldData[row][column + 1] &&  fieldData[row][column + 1] ==  fieldData[row][column + 2] {
                    fieldData[row][column  ].valid = false
                    fieldData[row][column+1].valid = false
                    fieldData[row][column+2].valid = false
                }
            }
        }
    }
    
    func decode(location: Int) -> (Int, Int) {
        let column = location % dimension[1]
        let row = (location - column) / dimension[0]
        return (row, column)
    }
    
    func toFile() -> String {
        var file = ""
        
        file += "\(dimension[0]) \(dimension[1])\n"
        
        for column in 0..<dimension[0] {
            var rowString = ""
            for row in 0..<dimension[1] {
                let cell =  fieldData[column][row]
                if      cell.type == CellType.none  { rowString += "?" }
                else if cell.type == CellType.white { rowString += "W" }
                else if cell.type == CellType.black { rowString += "B" }
            }
            file += "\(rowString)\n"
        }
        
        return file
    }
    
    func charAt(row: Int, column: Int) -> Character {
        let cell = fieldData[row][column]
        
        if cell.type == .black {
            return "b"
        } else if cell.type == .white {
            return "w"
        } else {
            return "?"
        }
    }
    func at(row: Int, column: Int) -> Cell {
        return fieldData[row][column]
    }
    mutating func set(at row: Int, column: Int, cell: Cell) {
        fieldData[row][column] = cell
    }
    
    mutating func setPressed(row: Int, column: Int, state: Bool) {
        if !fieldData[row][column].locked {
            fieldData[row][column].pressed = state
        }
    }
    
    mutating func toggleCell(row: Int, column: Int) {
        if !fieldData[row][column].locked {
            var type = fieldData[row][column].type
            switch type {
            case CellType.none:
                type = CellType.white
                break
            case CellType.white:
                type = CellType.black
                break
            case CellType.black:
                type = CellType.none
                break
            }
            fieldData[row][column].type = type
        }
        validate()
    }
    
    static func ==(lhs: Field, rhs: Field) -> Bool {
        if lhs.dimension != rhs.dimension {
            return false
        }
        var eq = true
        for row in 0..<lhs.dimension[0] {
            for column in 0..<lhs.dimension[0] {
                eq = rhs.at(row: row, column: column).type == lhs.at(row: row, column: column).type
            }
        }
        return eq
    }
}

struct Cell: Equatable {
    var type: CellType = CellType.none
    var locked: Bool = false
    var valid: Bool = true
    var pressed: Bool = false
    
    static func ==(lhs: Cell, rhs: Cell) -> Bool {
        return (lhs.type == rhs.type) && lhs.type != CellType.none && rhs.type != CellType.none
    }
}

enum CellType {
    case white
    case black
    case none
}

class Unruly {
    var field: Field
    var originalField: Field
    
    init(columns: Int, rows: Int, prefill: Bool) {
        field = Field(dimension: [columns, rows], fieldData: [])
        originalField = Field(dimension: [], fieldData: [])
        assert(columns % 2 == 0 && rows % 2 == 0, "Assertion failed: Dimension not a multiple of 2: [columns: \(columns), rows: \(rows)]")
        
        for _ in 0..<columns {
            var rowArray: [Cell] = []
            for _ in 0..<rows {
                rowArray.append(Cell(type: CellType.none, locked: false, valid: true, pressed: false))
            }
            field.fieldData.append(rowArray)
        }
        
//        if prefill {
//            generate()
//        }
        originalField = field
    }
    init(filepath: URL) {
        field = Field(dimension: [], fieldData: [])
        
        var filecontent = ""
        do    { filecontent = try String(contentsOf: filepath) }
        catch { print("Error: filepath " + filepath.absoluteString + " is not valid!") }
        
        var lines:[String] = []
        
        filecontent.enumerateLines(invoking: { line, _ in lines.append(line) })
        
        let sizes = lines[0].split(separator: " ")
        assert((sizes.count == 2), "Assertion failed: File Header is invalid")
        
        let width = Int(sizes[0]) ?? 1
        let height = Int(sizes[1]) ?? 1
        
        if (width % 2 != 0 || height % 2 != 0) {
            print("Error: Field dimension is invalid")
        }
        
        for line in lines[1...] {
            if line.count != width {
                print("Error: Field file line [\(line)] has incorrect number of characters")
            }
            
            var row: [Cell] = []
            line.forEach({ c in
                assert(["?", "W", "w", "B", "b"].contains(c), "Assertion failed: Found invalid cell [\(c)]")
                
                var cell: Cell = Cell(type: CellType.none, locked: false, valid: true, pressed: false)
                if      c == "W" || c == "w" { cell = Cell(type: CellType.white, locked: true, valid: true, pressed: false) }
                else if c == "B" || c == "b" { cell = Cell(type: CellType.black, locked: true, valid: true, pressed: false) }
                
                row.append(cell)
            })
            field.fieldData.append(row)
        }
            
        field.dimension = [height, width]
        originalField = field
    }
    
    func reset() {
        field = originalField
    }
    
//    func generate() {
//        func decode(location: Int, dimension: [Int]) -> (Int, Int) {
//            let column = location % field.dimension[1]
//            let row = (location - column) / field.dimension[0]
//            return (row, column)
//        }
//
//        func random(range: Range<UInt32>) -> Int {
//            return Int(range.startIndex + arc4random_uniform(range.endIndex - range.startIndex + 1))
//        }
//
//        var loop = true
//        while loop {
//            let cellPosition = decode(location: random(range: 1..<UInt32((field.dimension[0] * field.dimension[1])-1)), dimension: field.dimension)
//
//            let cellType = Bool.random()
//
//            field.fieldData[cellPosition.0][cellPosition.1] = Cell(type: cellType ? .white : .black, locked: true, valid: true, pressed: false)
//
//            let res = solve(allSolutions: true, measure: false)
//            let solvedField = res.0
//            let solutions = res.1
//
//            if solutions == 0 || solvedField == nil{
//                field.fieldData[cellPosition.0][cellPosition.1] = Cell(type: .none, locked: false, valid: false, pressed: false)
//            } else if solutions == 1 && solvedField != nil {
//                loop = false
//                field = solvedField!
//            }
//        }
//        field.validate()
//    }
    
    func solve(allSolutions: Bool = false, measure: Bool = false, solveOriginalField: Bool = false) -> (Field?, Int) {
        func deref(picosat: OpaquePointer?, numLits: Int32) -> [Int] {
            var solution: [Int] = []
            for lit in 1...numLits {
                let dereflit = picosat_deref(picosat, lit)
                
                solution.append(Int(lit * dereflit))
            }
            return solution
        }
        
        func nextSolution(picosat: OpaquePointer?, numLits: Int32) -> [Int]? {
            let sat = picosat_sat(picosat, -1)
            var solution: [Int]?
            
            if sat == PICOSAT_SATISFIABLE {
                solution = deref(picosat: picosat, numLits: numLits)
                
                /** block solution for future SAT **/
                var sol: [Int32] = Array<Int32>(repeating: 0, count: Int(numLits + 1))
                for i in 1...Int(numLits) {
                    let deref = picosat_deref(picosat, Int32(i))
                    sol[i] = deref == 1 ? 1 : -1 //cast 1 -> 1; 0 & -1 -> -1
                }
                
                var blocklits: [Int32] = []
                for i in 1...numLits {
                    let blocklit = i * (sol[Int(i)] == 1 ? -1 : 1) //invert literal based on solution
                    blocklits.append(blocklit)
                }
                blocklits.append(0)
                picosat_add_lits(picosat, &blocklits)
            }
            return solution
        }
        
        var fieldToSolve: Field? = solveOriginalField ? originalField : field

        let cnf = fieldToSolve!.toCNF()

        let picosat = picosat_init()
        for clause in cnf {
            var clauseTerm = clause
            clauseTerm.append(0)
            picosat_add_lits(picosat, &clauseTerm)
        }

        let start = DispatchTime.now()
        
        var solutions: [[Int]] = []
        var solution: [Int]? = []
    
        var next = true
        while next {
            solution = nextSolution(picosat: picosat, numLits: Int32(fieldToSolve!.dimension[0] * fieldToSolve!.dimension[1]))
            
            if solution != nil {
                solutions.append(solution!)
                next = solutions.count < 2
            } else {
                next = false
            }
            
            if !allSolutions {
                next = false
            }
        }
        let end = DispatchTime.now()
        
        if solutions.first != nil {
            fieldToSolve!.fromCNF(solution: solutions.first!)
        } else {
            fieldToSolve = nil
        }
        

        if !measure {
            print("Picosat took \(Double(end.uptimeNanoseconds - start.uptimeNanoseconds) / 1e6) ms")
        }
        
        picosat_reset(picosat)
        return (fieldToSolve, solutions.count)
    }
}

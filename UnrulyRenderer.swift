//
//  UnrulyRenderer.swift
//  Unruly
//
//  Created by Philipp Friese on 27.10.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

import Foundation
import Cocoa

class UnrulyRenderer: NSView
{
    var unruly: Unruly?
    
    override func mouseDown(with event: NSEvent) {
        let dimension = unruly?.field.dimension ?? [0,0]
        
        let location = self.convert(event.locationInWindow, from: nil)
        
        let height = Double(self.frame.height) / Double(unruly?.field.dimension[1] ?? 1)
        let width = Double(self.frame.width) / Double(unruly?.field.dimension[0] ?? 1)
        
        let row = ((unruly?.field.dimension[1] ?? 0) - Int(floor(CGFloat(location.y) / CGFloat(height)))) - 1
        let column = Int(floor(CGFloat(location.x) / CGFloat(width)))
        
        if row < 0 || row >= dimension[0] || column < 0 || column >= dimension[1] {
            return
        }
        
        unruly?.field.setPressed(row: row, column: column, state: true)
        
        self.needsDisplay = true
    }
    
    override func mouseUp(with event: NSEvent) {
        let dimension = unruly?.field.dimension ?? [0,0]
        
        let location = self.convert(event.locationInWindow, from: nil)
        
        let height = Double(self.frame.height) / Double(unruly?.field.dimension[1] ?? 1)
        let width = Double(self.frame.width) / Double(unruly?.field.dimension[0] ?? 1)
        
        let row = ((unruly?.field.dimension[1] ?? 0) - Int(floor(CGFloat(location.y) / CGFloat(height)))) - 1
        let column = Int(floor(CGFloat(location.x) / CGFloat(width)))
        
        if row < 0 || row >= dimension[0] || column < 0 || column >= dimension[1] {
            return
        }
        
        unruly?.field.setPressed(row: row, column: column, state: false)
        unruly?.field.toggleCell(row: row, column: column)
        
        self.needsDisplay = true
    }
    
    func render(width: Double, height: Double, rowIndex: Int, cellIndex: Int, cell: Cell) {
        
        let x = Double(cellIndex) * height
        let y = (Double(unruly?.field.dimension[0] ?? 0) - Double(rowIndex) - 1.0) * width
        
        /** Render black background **/
        if cell.type == .black {
            NSColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1).set()
        } else {
            NSColor(red: 0.25, green: 0.25, blue: 0.25, alpha: 1).set()
        }
        let backgroundSize = 0.99
        let backgroundPadding = (1 - backgroundSize) / 2
        let backgroundRect = CGRect(x: x + (height * backgroundPadding), y: y + (width * backgroundPadding), width: (width * backgroundSize), height: (height * backgroundSize))
        let background = NSBezierPath(rect: backgroundRect)
        background.fill()
        background.stroke()
        
        /** Render outer part **/
        let outerSize = cell.pressed ? 0.95 : 0.99
        let outerPadding = (1 - outerSize) / 2
        
        var outerColor = NSColor.white
        let modifier = CGFloat(cell.locked ? 0.0 : 0.0)
        switch cell.type {
        case CellType.white:
            outerColor = NSColor(red: 0.75 - modifier, green: 0.75 - modifier, blue: 0.75 - modifier, alpha: 1)
        case CellType.black:
            outerColor = NSColor(red: 0.2 - modifier, green: 0.2 - modifier, blue: 0.2 - modifier, alpha: 1)
        case CellType.none:
            outerColor = NSColor(red: 0.45 - modifier, green: 0.45 - modifier, blue: 0.45 - modifier, alpha: 1)
        }
        outerColor.set()
        let outerRect = CGRect(x: x + (height * outerPadding), y: y + (width * outerPadding), width: (width * outerSize), height: (height * outerSize))
        let outerFigure = NSBezierPath(rect: outerRect)
        outerFigure.fill()
        outerFigure.stroke()
        
        
        /** Render Cross **/
        let a0x = x
        let a0y = y
        let a1x = x + width
        let a1y = y + height
        
        let b0x = x + width
        let b0y = y
        let b1x = x
        let b1y = y + height
        
        let aPath = NSBezierPath()
        aPath.move(to: .init(x: a0x, y: a0y))
        aPath.line(to: .init(x: a1x, y: a1y))
        
        let bPath = NSBezierPath()
        bPath.move(to: .init(x: b0x, y: b0y))
        bPath.line(to: .init(x: b1x, y: b1y))
        
        var crossColor = NSColor.white
        switch cell.type {
        case CellType.white:
            crossColor = NSColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        case CellType.black:
            crossColor = NSColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
        case CellType.none:
            crossColor = NSColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1)
        }
        
        crossColor.set()
        aPath.stroke()
        aPath.fill()
        
        bPath.stroke()
        bPath.fill()
        
        /** Render inner part **/
        let innerSize = cell.pressed ? 0.68 : 0.7
        let innerPadding = (1 - innerSize) / 2
        
        var innerColor = NSColor.white
        switch cell.type {
        case CellType.white:
            innerColor = NSColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        case CellType.black:
            innerColor = NSColor(red: 0.25, green: 0.25, blue: 0.25, alpha: 1)
        case CellType.none:
            innerColor = NSColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        }
        
        innerColor.set()
        let innerRect = CGRect(x: x + (height * innerPadding), y: y + (width * innerPadding), width: (width * innerSize), height: (height * innerSize))
        let innerFigure = NSBezierPath(rect: innerRect)
        innerFigure.fill()
        innerFigure.stroke()
        
        
        
        /** Render inner border **/
        crossColor.set()
        let innerBorderRect = CGRect(x: x + (height * innerPadding), y: y + (width * innerPadding), width: (width * innerSize), height: (height * innerSize))
        let innerBorderFigure = NSBezierPath(rect: innerBorderRect)
        innerBorderFigure.stroke()
        
        
        
        /** Border & Invalid Marker **/
        let borderSize = 0.99
        let borderPadding = (1 - borderSize) / 2
        let borderRect = CGRect(x: x + (height * borderPadding), y: y + (width * borderPadding), width: (width * borderSize), height: (height * borderSize))
        let borderFigure = NSBezierPath(rect: borderRect)
        if !cell.valid {
            NSColor.red.set()
        } else {
            NSColor(red: 0.25, green: 0.25, blue: 0.25, alpha: 1).set()
        }
        borderFigure.stroke()
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
  
        guard let guardedUnruly = unruly else {
            print("Error: Unruly is nil!")
            return
        }
        
        let width = Double(self.frame.width) / Double(guardedUnruly.field.dimension[0])
        let height = Double(self.frame.height) / Double(guardedUnruly.field.dimension[1])
    
        for (rowIndex, row) in guardedUnruly.field.fieldData.enumerated() {
            for (cellIndex, cell) in row.enumerated() {
                render(width: Double(width), height: Double(height), rowIndex: rowIndex, cellIndex: cellIndex, cell: cell)
            }
        }
    }
}

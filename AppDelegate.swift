//
//  AppDelegate.swift
//  Unruly
//
//  Created by Philipp Friese on 27.10.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    var newGameVC: NewGameViewController? = nil
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }
    
    @objc func generateGame() {
        guard let vc = NSApplication.shared.mainWindow?.contentViewController as? ViewController else {
            print("Error while casting mainWindow's contentViewController")
            return
        }

        if newGameVC == nil {
            print("Error: newGameVC is nil")
            return
        }
        
        let columns = newGameVC!.columnsInput.integerValue
        let rows = newGameVC!.rowsInput.integerValue
        let prefill = newGameVC!.prefillToggle?.state == NSControl.StateValue.on ? true : false
        
        newGameVC!.dismiss(nil)
        
        vc.unruly = Unruly(columns: columns, rows: rows, prefill: prefill)
        vc.updateUnrulyRenderer()
    }

    @IBAction func newGame(_ sender: Any) {
        newGameVC = NSStoryboard(name: "Main", bundle: nil).instantiateController(withIdentifier: "NewGameID") as? NewGameViewController

        guard let vc = NSApplication.shared.mainWindow?.contentViewController as? ViewController else {
            print("Error while casting mainWindow's contentViewController")
            return
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(generateGame), name: .init("NewGameGenerate"), object: nil)
        
        vc.presentAsSheet(newGameVC!)
    }
  
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}


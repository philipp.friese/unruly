//
//  ViewController.swift
//  Unruly
//
//  Created by Philipp Friese on 27.10.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

import Cocoa
import AppKit

class ViewController: NSViewController {
    
    @IBOutlet weak var unrulyView: UnrulyRenderer!
    
    var unruly: Unruly?
    
    @IBAction func openDocument(_ sender: Any?) {
        let dialog = NSOpenPanel()
        dialog.title = "Select .unruly file"
        dialog.allowedFileTypes = ["unruly", "txt"]
        
        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            initializeUnruly(path: dialog.url!)
        } else {
            return
        }
    }
    
    @IBAction func reset(_ sender: Any) {
        unruly!.reset()
        updateUnrulyRenderer()
    }
    
    @IBAction func run(_ sender: Any) {
        
        let result = unruly?.solve(allSolutions: true, measure: false, solveOriginalField: false)
        
        if result == nil || result!.1 == 0 || result!.0 == nil {
            let alert = NSAlert()
            alert.informativeText = "Please try a different assignment or try resetting the field."
            alert.messageText = "Error: Given Unruly Field is not solvable!"
            alert.alertStyle = .critical
            alert.addButton(withTitle: "Cancel")
            alert.runModal()
            return
        } else {
            unruly!.field = result!.0!
            updateUnrulyRenderer()
        }
    }
    
    @IBAction func save(_ sender: Any?) {
        
        let solvable = (unruly?.solve(allSolutions: false, measure: false, solveOriginalField: false).1 ?? 0 > 0)
        
        if !solvable {
            let alert = NSAlert()
            alert.informativeText = "Please try a different assignment or try resetting the field."
            alert.messageText = "Error: Given Unruly Field is not solvable!"
            alert.alertStyle = .critical
            alert.addButton(withTitle: "Cancel")
            alert.runModal()
            return
        }
        
        guard let file = unruly?.field.toFile() else {
            print("Error: Field object invalid")
            return
        }
        
        let savePanel = NSSavePanel()
        savePanel.allowedFileTypes = ["unruly","txt"]
        
        savePanel.begin(completionHandler: { (result) -> Void in
            if result == NSApplication.ModalResponse.OK {
                    let filename = savePanel.url
                    do    { try file.write(to: filename!, atomically: true, encoding: String.Encoding.utf8) }
                    catch { print("Error: Could not write file to \(String(describing: filename)): \(error)") }
            }})
    }


    
    func initializeUnruly(path: URL) {
        unruly = Unruly(filepath: path)
        
        updateUnrulyRenderer()
    }
    
    func updateUnrulyRenderer() {
        if unruly != nil {
            unrulyView.unruly = unruly!
        }
        unrulyView.needsDisplay = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        unruly = Unruly(columns: 8, rows: 8, prefill: false)
        updateUnrulyRenderer()
        
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
}


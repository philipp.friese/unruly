//
//  NewGameViewController.swift
//  Unruly
//
//  Created by Philipp Friese on 28.10.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

import Cocoa

class NewGameViewController: NSViewController {
    
    @IBOutlet weak var columnsInput: NSTextField!
    @IBAction func columnInputChanged(_ sender: NSTextField) {
        columnStepper.integerValue = sender.integerValue
    }
    
    @IBOutlet weak var columnStepper: NSStepper!
    @IBAction func columnStep(_ sender: NSStepper) {
        columnsInput.integerValue = sender.integerValue
        
    }
    @IBOutlet weak var rowsInput: NSTextField!
    @IBAction func rowInputChanged(_ sender: NSTextField) {
        rowStepper.integerValue = sender.integerValue
    }

    @IBOutlet weak var rowStepper: NSStepper!
    @IBAction func rowStep(_ sender: NSStepper) {
        rowsInput.integerValue = sender.integerValue
    }

    @IBOutlet weak var prefillToggle: NSButton!
    @IBAction func pushCreate(_ sender: Any) {
        NotificationCenter.default.post(name: .init("NewGameGenerate"), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
